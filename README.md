# pgPulse #

pgPulse is a snapshotting tool that helps PostgreSQL gather and store its history. Oracle like databases have a feature using which we can see the activity in the database at a certain point in time.
This helps use being more predective than being proactive and reactive while managing PostgreSQL Databases. 
Unlike other databases, this tool gives a feature that allows you to capture such historic information in a remote centralized database. Thus, avoids a huge Write IO on the Production Database for which the data is being collected.

This tool help you query the historic data and understand the SQL's that have performed bad, tables/indexes accessed, locks acquired, etc on the PostgreSQL database at a certain time. 

### Version ###

Version : 1.0

### How do I get set up? ###

Step 1:
========

Download pgPulse and install the required Python Modules

Perform these steps on the Server where you want to store the snapshots. 

On Redhat/CentOS/OEL
====================
yum install git 
git clone <pgpulserepo>
yum install python-setuptools
sudo easy_install pip
pip install psycopg2
pip install psutils

On Ubuntu/Debian
====================
apt-get install git 
git clone <pgpulserepo>
apt-get install python-pip
pip install psycopg2
pip install psutils

Step 2:
========

Modify the databaseconfig.json file with the credentials of Database to be Monitored and the Database to be Stored with the snapshotting information.

{
    "db_to_monitor":{
        "host":"192.168.0.1",
        "user":"postgres",
        "passwd":"oracle",
        "db":"sammy",
	"port":"5432"
    },
	"db_to_store":{
        "host":"192.168.0.2",
        "user":"postgres",
        "passwd":"oracle",
        "db":"naga",
	"port":"5432",
	"schema":"avinash"
    }
}

In the above example, 
192.168.0.1 --> Is the IP Address of the Database Server being monitored.
192.168.0.2 --> Is the IP Address of the Database Server where the snapshots are being stored for the above Server.

### Contribution guidelines ###

Author : Avinash Vallarapu
Reviewer/Testing : Jobin Augustine

### Who do I talk to? ###

Avinash Vallarapu --> avinash.vallarapu@gmail.com or avinash.vallarapu@openscg.com